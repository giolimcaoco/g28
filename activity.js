/*
Activity:
1. Create an activity.js file on where to write and save the solution for the activity.
2. Create a database of hotel with a collection of rooms.
3. Insert a single room (insertOne method) with the following details:

- name - single
- accomodates - 2
- price - 1000
- description - A simple room with all the basic necessities
- rooms_available - 10
- isAvailable - false

4. Insert multiple rooms (insertMany method) with the following details:

- name - double
- accomodates - 3
- price - 2000
- description - A room fit for a small family going on a vacation
- rooms_available - 5
- isAvailable - false

- accomodates - 4
- price - 4000
- description - A room with a queen sized bed perfect for a simple getaway
- rooms_available - 15
- isAvailable - false

5. Use the find method to search for a room with the name double.
6. Use the updateOne method to update the queen room and set the available rooms to 0.
7. Use the deleteMany method rooms to delete all rooms that have 0 availability.
8. Create a git repository named S28.
9. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
10. Add the link in Boodle.
*/

// .insertOne ();
db.rooms.insertOne({
    name: "single", 
    accomodate: 2, 
    price: 1000, 
    description: "A simple room with all the basic necessities",
    rooms_available: 10,
    isAvailable: false
 });

// .insertMany ();
db.rooms.insertMany([
    {
        name: "double", 
        accomodate: 3, 
        price: 2000, 
        description: "A room fit for a small family going on a vacation",
        rooms_available: 5,
        isAvailable: false
    },
    {   
        name: "queen", 
        accomodate: 4, 
        price: 4000, 
        description: "A room with a queen sized bed perfect for a simple getaway",
        rooms_available: 15,
        isAvailable: false
    }
]);

// .find ();
db.rooms.find({name:"double"});

// .updateOne();
db.rooms.updateOne(
    { name: "queen" },
    {
        $set:  {   
        name: "queen", 
        accomodate: 4, 
        price: 4000, 
        description: "A room with a queen sized bed perfect for a simple getaway",
        rooms_available: 0,
        isAvailable: false
        }   
    }
);

// .deleteMany():
db.rooms.deleteMany({rooms_available:0});